<?php

class pav
{
	
	private $id;
  
	
	private $name;
	
	private $city;
	
	private $borough;
	
	private $status;
	

	public function __construct()
	{
		include_once("Query/pavQueries.php")
	}

	
	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
	}
	

	
	public function getName()
	{
	
		return $this->name;
	
	}

	public function setName($name)
	{
		$this->name = $name;
	}
	
	
	public function getCity()
	{
	
		return $this->city;
	
	}

	public function setCity($city)
	{
		$this->city = $city;
	}
	
	
	public function getBorough()
	{
	
		return $this->borough;
	
	}

	public function setBorough($borough)
	{
		$this->borough = $borough;
	}
	
	
	public function getStatus()
	{
	
		return $this->status;
	
	}

	public function setStatus($status)
	{
		$this->status = $status;
	}
	
	

	public static function getPavById($id)
	{
		$name = "Pav/QueryGetUserById";
		$query = constant($name);
		try 
		{	
			$conn = $GLOBAL["conn"];
			$stmt = $conn->prepate($query);
			$stmt->execute([$id]);
		  return $stmt->fetchObject(__CLASS__);
		}
		catch (PDOException $e) 
		{
			echo "PDOException";
			print $e->getMessage();
		}
	}

	public static function getAll()
	{
		$name = "Pav/QueryGetAll";
		$query = constant($name);
		try 
		{	
			$conn = $GLOBAL["conn"];
			$stmt = $conn->query($query);
			$entities = array();
			while ($entity = $stmt->fetchObject(__CLASS__)) 
			{
				$entities[] = $entity;
			}
			return $entities;
		}
		catch (PDOException $e) 
		{
			echo "PDOException";
			print $e->getMessage();
		}
	}

	public static function getAllTop($top)
	{
		$name = "Pav/QueryGetAllTop";
		$query = constant($name);
		try 
		{	
			$conn = $GLOBAL["conn"];
			$stmt = $conn->prepare($query);
			$stmt->bindValue(1, $top, PDO::PARAM_INT);
			$stmt->execute();
			$entities = array();
			while ($entity = $stmt->fetchObject(__CLASS__)) 
			{
				$entities[] = $entity;
			}
			return $entities;
		}
		catch (PDOException $e) 
		{
			echo "PDOException";
			print $e->getMessage();
		}
	}

	public function save()
	{
		$name = "Pav/QueryInsert";
		$query = constant($name);
		try 
		{	
			$conn = $GLOBAL["conn"];
			$stmt = $conn->prepare($query);
			
			
			
			$stmt->bindValue(1, $this->name, PDO::PARAM_STR);
			
			
			
			$stmt->bindValue(2, $this->city, PDO::PARAM_STR);
			
			
			
			$stmt->bindValue(3, $this->borough, PDO::PARAM_STR);
			
			
			
			$stmt->bindValue(4, $this->status, PDO::PARAM_STR);
			
			

			return $prep->execute();
		}
		catch (PDOException $e) 
		{
			echo "PDOException";
			print $e->getMessage();
		}
	}
	
}

?>