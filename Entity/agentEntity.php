<?php

class agent
{
	
	private $id;
  
	
	private $firstname;
	
	private $lastname;
	
	private $status;
	

	public function __construct()
	{
		include_once("Query/agentQueries.php")
	}

	
	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
	}
	

	
	public function getFirstname()
	{
	
		return $this->firstname;
	
	}

	public function setFirstname($firstname)
	{
		$this->firstname = $firstname;
	}
	
	
	public function getLastname()
	{
	
		return $this->lastname;
	
	}

	public function setLastname($lastname)
	{
		$this->lastname = $lastname;
	}
	
	
	public function getStatus()
	{
	
		return $this->status;
	
	}

	public function setStatus($status)
	{
		$this->status = $status;
	}
	
	

	public static function getAgentById($id)
	{
		$name = "Agent/QueryGetUserById";
		$query = constant($name);
		try 
		{	
			$conn = $GLOBAL["conn"];
			$stmt = $conn->prepate($query);
			$stmt->execute([$id]);
		  return $stmt->fetchObject(__CLASS__);
		}
		catch (PDOException $e) 
		{
			echo "PDOException";
			print $e->getMessage();
		}
	}

	public static function getAll()
	{
		$name = "Agent/QueryGetAll";
		$query = constant($name);
		try 
		{	
			$conn = $GLOBAL["conn"];
			$stmt = $conn->query($query);
			$entities = array();
			while ($entity = $stmt->fetchObject(__CLASS__)) 
			{
				$entities[] = $entity;
			}
			return $entities;
		}
		catch (PDOException $e) 
		{
			echo "PDOException";
			print $e->getMessage();
		}
	}

	public static function getAllTop($top)
	{
		$name = "Agent/QueryGetAllTop";
		$query = constant($name);
		try 
		{	
			$conn = $GLOBAL["conn"];
			$stmt = $conn->prepare($query);
			$stmt->bindValue(1, $top, PDO::PARAM_INT);
			$stmt->execute();
			$entities = array();
			while ($entity = $stmt->fetchObject(__CLASS__)) 
			{
				$entities[] = $entity;
			}
			return $entities;
		}
		catch (PDOException $e) 
		{
			echo "PDOException";
			print $e->getMessage();
		}
	}

	public function save()
	{
		$name = "Agent/QueryInsert";
		$query = constant($name);
		try 
		{	
			$conn = $GLOBAL["conn"];
			$stmt = $conn->prepare($query);
			
			
			
			$stmt->bindValue(1, $this->firstname, PDO::PARAM_STR);
			
			
			
			$stmt->bindValue(2, $this->lastname, PDO::PARAM_STR);
			
			
			
			$stmt->bindValue(3, $this->status, PDO::PARAM_STR);
			
			

			return $prep->execute();
		}
		catch (PDOException $e) 
		{
			echo "PDOException";
			print $e->getMessage();
		}
	}
	
}

?>