<?php

class tour
{
	
	private $id;
  
	
	private $name;
	

	public function __construct()
	{
		include_once("Query/tourQueries.php")
	}

	
	public function getId()
	{
		return $this->id;
	}

	public function setId($id)
	{
		$this->id = $id;
	}
	

	
	public function getName()
	{
	
		return $this->name;
	
	}

	public function setName($name)
	{
		$this->name = $name;
	}
	
	

	public static function getTourById($id)
	{
		$name = "Tour/QueryGetUserById";
		$query = constant($name);
		try 
		{	
			$conn = $GLOBAL["conn"];
			$stmt = $conn->prepate($query);
			$stmt->execute([$id]);
		  return $stmt->fetchObject(__CLASS__);
		}
		catch (PDOException $e) 
		{
			echo "PDOException";
			print $e->getMessage();
		}
	}

	public static function getAll()
	{
		$name = "Tour/QueryGetAll";
		$query = constant($name);
		try 
		{	
			$conn = $GLOBAL["conn"];
			$stmt = $conn->query($query);
			$entities = array();
			while ($entity = $stmt->fetchObject(__CLASS__)) 
			{
				$entities[] = $entity;
			}
			return $entities;
		}
		catch (PDOException $e) 
		{
			echo "PDOException";
			print $e->getMessage();
		}
	}

	public static function getAllTop($top)
	{
		$name = "Tour/QueryGetAllTop";
		$query = constant($name);
		try 
		{	
			$conn = $GLOBAL["conn"];
			$stmt = $conn->prepare($query);
			$stmt->bindValue(1, $top, PDO::PARAM_INT);
			$stmt->execute();
			$entities = array();
			while ($entity = $stmt->fetchObject(__CLASS__)) 
			{
				$entities[] = $entity;
			}
			return $entities;
		}
		catch (PDOException $e) 
		{
			echo "PDOException";
			print $e->getMessage();
		}
	}

	public function save()
	{
		$name = "Tour/QueryInsert";
		$query = constant($name);
		try 
		{	
			$conn = $GLOBAL["conn"];
			$stmt = $conn->prepare($query);
			
			
			
			$stmt->bindValue(1, $this->name, PDO::PARAM_STR);
			
			

			return $prep->execute();
		}
		catch (PDOException $e) 
		{
			echo "PDOException";
			print $e->getMessage();
		}
	}
	
}

?>